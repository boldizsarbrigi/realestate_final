package com.sda.brigi.advanced.RealEstateMain;
//B.  Develop an application that reads your listings.txt file, analyzes the properties listed, and outputs an overview of properties listed to an 

//overview.txt file. Your application should do the following:

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.TreeSet;

import javax.swing.plaf.synth.SynthSeparatorUI;

import com.sda.brigi.advanced.RealEstate.Listing;

public class ApplicationB {

	public static void main(String[] args) throws IOException {

		// 1. Prompt the user for the name of the input file (listings.txt).
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter listings.txt file name");
		String filename = scanner.nextLine();

		List<Listing> properyList = new ArrayList<>(); // list of listings.txt content
		TreeSet<Double> setPropertyValue = new TreeSet<Double>(); // set containing value of the property
		List<Integer> properyIdList = new ArrayList<>(); // list containing the id's of the property

		// 2. Open the listings.txt file and read in property listing information using
		// a buffered FileReader.
		BufferedReader objReader = null;
		try {
			String line;
			objReader = new BufferedReader(
					new FileReader("C:/Users/Brigitta/Desktop/SDA_Workspace/RealEstate/src/" + filename));
			while ((line = objReader.readLine()) != null) {
				String[] array = line.split(" ");
				// 3. Count the total number of property listings for sale.
				Listing listing = new Listing();
				listing.setIdProperty(Integer.parseInt(array[0]));
				properyList.add(listing);

				// 4. Calculate the total value of property for sale.
				listing.setPriceProperty(Double.parseDouble(array[2]));
				setPropertyValue.add(listing.getPriceProperty());

				// 5. Store each property id into an ArrayList.
				properyIdList.add(listing.getIdProperty());

			}
			// 3. Count the total number of property listings for sale.
			System.out.println("Total number of property listings for sale: " + properyList.size());

			int totalNumberOfProperties = properyList.size();

			// 3.1 Use buffered FileWriter to write the count of the number of property
			// listings to your overview.txt file.
			FileWriter writer = new FileWriter(
					"C:/Users/Brigitta/Desktop/SDA_Workspace/RealEstate/src/" + "overview.txt");
			BufferedWriter output = new BufferedWriter(writer);
			output.write("Total properties listed: " + totalNumberOfProperties);

			// 4. Calculate the total value of property for sale.
			double sumPropertyValue = 0;
			for (double value : setPropertyValue) {
				sumPropertyValue = sumPropertyValue + value;
			}
			// 4. Calculate the total value of property for sale.
			System.out.println("Total value of properties listed: " + sumPropertyValue);
			output.write("\n");
			// 4.1 Use a buffered FileWriter to write the total value of properties
			// currently for sale.
			output.write("Total value of properties listed: " + sumPropertyValue);
			output.write("\n");

			// 5. Store each property id into an ArrayList.
			System.out.println("Property list by id before sorting: ");
			for (int property : properyIdList) {
				System.out.println(property);
			}
			output.write("\n");
			System.out.println("Property list by id after sorting: ");
			Collections.sort(properyIdList);
			for (Integer property : properyIdList) {
				System.out.println(property);
				output.write(property+ "\n");
			}	
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (objReader != null)
					objReader.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
