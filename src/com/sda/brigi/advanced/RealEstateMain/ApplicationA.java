package com.sda.brigi.advanced.RealEstateMain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.sda.brigi.advanced.RealEstate.Listing;
import com.sda.brigi.advanced.RealEstate.PropertyType;

public class ApplicationA {

	public static void main(String[] args) throws IOException {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter input file name: ");
		String filename = scanner.nextLine();
		System.out.println(filename);

		File file = new File("C:/Users/Brigitta/Desktop/SDA_Workspace/RealEstate/src/" + filename);

		TreeSet<String> setHash = new TreeSet<String>();
		List<Listing> list = new ArrayList<>();
		Map<Integer, Double> map = new TreeMap<>();
		Set<Listing> setHashListing = new HashSet<>();
		Map<Listing, Double> mapListing = new TreeMap<>();

		try {
			Scanner read = new Scanner(file);
			while (read.hasNextLine()) {
				String line = read.nextLine();

				String[] array = line.split(" ");

				Listing listing = new Listing(); // se pune in while pentru a nu se crea o alta lista de fiecare date,
													// ci se updateaza in while

				listing.setIdProperty(Integer.parseInt(array[0]));

				listing.setPropertyType(PropertyType.valueOf(array[1].toUpperCase()));// cu valueOf convertim din enum
																						// in string, adica luam
																						// valoarea exacta, insa trebuie
																						// sa facem uppercase pt ca
																						// enumurile se uita exact la
																						// acelasi nume
				
				listing.setPropertyType(PropertyType.convertFromString(array[1]));
				
				PropertyType myPropertyType=PropertyType.FARM; // asa se defineste un property type
				myPropertyType.name();//to return the property type
				myPropertyType.getType();
				
				listing.setPriceProperty(Double.parseDouble(array[2]));
				listing.setIdAgent(Integer.parseInt(array[3]));

				// add listing
				list.add(listing);

				for (int i = 0; i < array.length; i++) {
				}
				// convert property type to uppercase
				String upperProperty = array[1].toUpperCase();
				// add uppercase property type to hashset
				setHash.add(upperProperty);
				setHashListing.add(listing);
				mapListing.put(listing, listing.getPriceProperty());
			}

			// compareTo method
			System.out.println("compareTo overload method testing " + list.get(0).compareTo(list.get(1)));

			System.out.println("Print setHash Listing");
			for (Listing printListHash : setHashListing) {
				System.out.println(printListHash);
			}
			// iterate hashset and print the elements
			System.out.println("HashSet propertype print changed to TreeSet for ordered list: ");
			for (String wordHash : setHash) {
				System.out.println(wordHash + " ");
			}
			// print list
			for (Listing printList : list) {
				System.out.println(printList);
				// add agentId and priceProperty to map
				// setHash.add(printList.getPropertyType());
				int agentId = printList.getIdAgent();
				if (map.containsKey(agentId)) {
					double newValue = map.get(agentId) + printList.getPriceProperty();
					map.put(agentId, newValue);
				} else {
					map.put(agentId, printList.getPriceProperty());
				}
			}
			for (Integer key : map.keySet()) {
				Double value = map.get(key);
				System.out.println(key + " " + value);
			}
			System.out.println("mapListing print: ");
			for (Listing key : mapListing.keySet()) {
				Double value = mapListing.get(key);
				System.out.println(key + " " + value);
			}

			// buffered file writing
			FileWriter fstream = new FileWriter(
					"C:/Users/Brigitta/Desktop/SDA_Workspace/RealEstate/src/" + "agentreport_buffered.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			// Iterator<String> it = setHash.iterator();
			// while (it.hasNext()) {
			for (String wordHash : setHash) {
				out.write(wordHash + "\n");
			}
			out.write("\n");
			for (Integer key : map.keySet()) {
				Double value = map.get(key);
				out.write(key + " " + value + "\n");
			}
			out.close();

			// unbuferred file writing
			PrintWriter pw = null;
			try {
				File fileUnBuffered = new File(
						"C:/Users/Brigitta/Desktop/SDA_Workspace/RealEstate/src/" + "agentreport_unbuffered.txt");
				FileWriter fw = new FileWriter(fileUnBuffered, true);
				pw = new PrintWriter(fw);
				for (String wordHash : setHash) {
					pw.print(wordHash + "\n");
				}
				pw.print("\n");
				for (Integer key : map.keySet()) {
					Double value = map.get(key);
					pw.print(key + " " + value + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (pw != null) {
					pw.close();
				}
			}

		} finally {
		}

		// 4.2 Create an agentreport.txt file.
		// 5. Use an Iterator to iterate through your Set and write your sorted set of
		// property types sold by the agents to the agentreport.txt file using
		// unbuffered file output stream.
		// FileWriter write =new FileWriter(agentFile);

	}
}
