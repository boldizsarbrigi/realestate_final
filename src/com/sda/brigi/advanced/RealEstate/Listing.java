package com.sda.brigi.advanced.RealEstate;
//4. Use a Map to calculate total property listed in dollars and cents for each agent id.

// Note: Agent id would be the key, and accumulated total of property listed
// would be the value.

public class Listing implements Comparable<Listing> {
	private int idProperty;
	private PropertyType propertyType;
	private double priceProperty;
	private int idAgent;

	// default constructor
	public Listing() {
	}

	// constructor cu 4 parametrii
	public Listing(int idProperty, PropertyType propertyType, double priceProperty, int idAgent) {
		this.idProperty = idProperty;
		this.propertyType = propertyType;
		this.priceProperty = priceProperty;
		this.idAgent = idAgent;

	}

	public int getIdProperty() {
		return idProperty;
	}

	public void setIdProperty(int idProperty) {
		this.idProperty = idProperty;
	}

	public PropertyType getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(PropertyType propertyType) {
		this.propertyType = propertyType;
	}

	public double getPriceProperty() {
		return priceProperty;
	}

	public void setPriceProperty(double priceProperty) {
		this.priceProperty = priceProperty;
	}

	public int getIdAgent() {
		return idAgent;
	}

	public void setIdAgent(int idAgent) {
		this.idAgent = idAgent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idAgent;
		result = prime * result + idProperty;
		long temp;
		temp = Double.doubleToLongBits(priceProperty);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((propertyType == null) ? 0 : propertyType.hashCode());
		return result;
	}

	//auto-generated equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Listing other = (Listing) obj;
		if (idAgent != other.idAgent)
			return false;
		if (idProperty != other.idProperty)
			return false;
		if (Double.doubleToLongBits(priceProperty) != Double.doubleToLongBits(other.priceProperty))
			return false;
		if (propertyType == null) {
			if (other.propertyType != null)
				return false;
		} else if (!propertyType.equals(other.propertyType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Listing [idProperty=" + idProperty + ", propertyType=" + propertyType + ", priceProperty="
				+ priceProperty + ", idAgent=" + idAgent + "]";
	}

	// override compareTo pentru a pune in ordine mapListing dupa priceProperty in
	// clasa
	// ApplicationA

	@Override
	public int compareTo(Listing o) {
		Listing listing = (Listing) o;
		if (this.equals(listing)) {
			return 0;
		} else {
			if (this.priceProperty > listing.priceProperty) {
				return 1;
			} else {
				return -1;
			}
		}

	}

	// metoda overloading-acelasi metoda cu lista de parametrii diferit pt. compareTo includeAgentId in lista de parametrii
	private int compareByAgentId(Listing o, boolean isDescending) {
		if (this.idAgent == o.idAgent) {
			return 0;
		} else if (this.idAgent > o.idAgent) {
			if (isDescending) {
				return -1;
			}
			return 1;
		} else {
			if (isDescending) {
				return 1;
			}
			return -1;
		}
	}

	public int compareTo(Listing o, boolean orderByAgentId, boolean isDescending) {
		if (orderByAgentId == true) {
			return compareByAgentId(o, isDescending);
		} else {
			return compareTo(o);
		}
	}

	public int compareTo(Listing o, boolean compareByAgentId) {
		if (compareByAgentId) {
			return compareByAgentId(o, false);
		} else {
			return compareTo(o);
		}
	}

}
