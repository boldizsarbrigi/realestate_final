package com.sda.brigi.advanced.RealEstate;

public enum PropertyType {

	COMMERCIAL("Commercial"), RESIDENTIAL("Residential"), LAND("Land"), FARM("Farm"), NO_MAND_LAND("None");

	private String type;

	private PropertyType(String type) {
		this.type = type;
	}

	public static PropertyType convertFromString(String type) {
		switch (type.toUpperCase()) {
		case "COMMERCIAL":
			return COMMERCIAL;
		case "RESIDENTIAL":
			return RESIDENTIAL;
		case "LAND":
			return LAND;
		case "FARM":
			return FARM;
		default:
			return NO_MAND_LAND;
		}
	}

	public String getType() {
		return type;

	}

}
